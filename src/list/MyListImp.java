package list;

import java.util.Arrays;

public class MyListImp<E> {

    static final int INITIAL_CAPACITY = 10;
    static final int ACTUAL_CAPACITY = 10;
    private int length;
    private Object[] array;

    public MyListImp() {
        this.array = new Object[INITIAL_CAPACITY];
        this.length = 0;
    }

    public int size() {
        return length;
    }

    // O(1)
    public boolean add(E e) {

        array[length] = e;
        length++;
        return true;
    }

    public E delete(int index) {
        E deleted = (E) array[index];
        updateIndexes(index);
        length--;
        return deleted;
    }

    private void updateIndexes(int index) {
        for (int i = index; i < array.length - 1; i++) {
            array[index] = array[index + 1];
            array[size() - 1] = null;

        }
    }

    @Override
    public String toString() {
        return "MyListImp{" +
                "length=" + length +
                ", array=" + Arrays.toString(array) +
                '}';
    }

    public E get(int index) {
        return (E) array[index];
    }
}
